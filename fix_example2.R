# By Gavin Rhys Lloyd

fcn = function(current=c(60,40),target=c(40,60)) { 
    
    # current is the existing mixture you want to adjust e.g. 60:40 collect = c(60,40). Must sum to 100.
    # target is the mixture you are aiming for e.g. 40:60 target = c(40,60). Must sum to 100.
    
    if (sum(current)!=100){
        warning('sum(current) != 100')
    }
    if (sum(target)!=100){
        warning('sum(target) != 100')
    }
    
    # keep original input
    orig=current
    
    # ensure only adding to mixture; smallest ratio first means all others will 
    # be an increase
    x = order(target/orig,decreasing = FALSE) 

    # loop over all unique pairs
    for (k in 1:(length(current)-1)) {
        for (j in (k+1):(length(current))) {

            # desired ratio between j and k based on target
            rat=target[x[j]]/target[x[k]]
            
            # if k is fixed, what value for j gives correct ratio?
            val = rat*current[x[k]]
            
            # update current with corrected value for this ratio so it gets used 
            # next time its needed
            current[x[j]]=val
            
        }
        
    }
    
    # amount to add of each is difference between input and updated
    toAdd=current-orig
    #    
    # # report
    print(paste0('To adjust a mixture from ',paste0(orig,collapse=':'),
        ' to ', paste0(target,collapse=':'),
        ' make the solution up to ',paste0(current,collapse=':'),
        ' by adding ', paste0(toAdd,collapse=':')))
    print(paste0("The final volume will be ", sum(current)/sum(orig), 'x larger.'))
    
    # return amount to add of each substance
    return(toAdd)
}
